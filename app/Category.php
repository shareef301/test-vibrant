<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Vibrant\Vibrant\Traits\Tablable;

class Category extends Model
{
    use Tablable;

    protected $searchable_fields = ['name' ];
    protected $available_fields = ['id', 'name', 'color', 'hour', 'day', 'created_at', 'updated_at'];
    protected $unsortable_fields = ['id'];
    protected $hidden_fields = ['created_at', 'updated_at'];

    public $form_fields = [
        'name' => ['row' => 'new', 'col_class' => 'col-sm-6', 'required' => true ],
        'color' => ['row' => 'same', 'col_class' => 'col-sm-6', 'required' => true ],
        'hour' => ['row' => 'new', 'input' => 'email', 'col_class' => 'col-sm-8', 'required' => true ],
        'day'=> ['row' => 'same', 'col_class' => 'col-sm-4' ],
        'id'=>['input' => 'range']
    ];
}
